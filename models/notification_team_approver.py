# -*- coding: utf-8 -*-
from odoo import fields, models
from datetime import datetime

from odoo.tools.safe_eval import pytz


class NotificationTeamApprover(models.Model):
    _inherit = "notification.team.approver"

    is_requisition = fields.Boolean(string="Requisition order")

    def send_notification(self):
        super(NotificationTeamApprover, self).send_notification()
        user_tz = pytz.timezone(self.env.context.get('tz') or self.env.user.tz or 'UTC')
        date_now = datetime.now().astimezone(user_tz)
        notification_users_ids = self.env["notification.team.approver"].search([
            '&', '&', ("active", "=", True),
            '&', ("is_requisition", "=", True),
            ("time", "=", date_now.hour),
            '|', ("frequency", "=", "daily"),
            ("days.code", "=", date_now.weekday())
        ]).mapped("user_id")
        for user in notification_users_ids:
            template = 'add_cron_notification_purchase_requisition.notification_purchases_requisition_to_approve'
            order_ids = self.env["purchase.requisition"].search([
                ("current_approver.user_id", "=", user.id)
            ])
            self.send_mail(template, user, order_ids)
