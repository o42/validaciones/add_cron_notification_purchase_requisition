# -*- coding: utf-8 -*-
{
    'name': 'Notificaciones cron acuerdo de compra(purchase_approval_route)',
    'version': '1.0.0',
    'summary': """
    Se agrega notificaciones de tareas
    """,
    'category': 'Purchases',
    'author': 'Odoo',
    'support': 'odoo',
    'license': 'OPL-1',
    'description':
        """
        Notificaciones
        """,
    'data': [
        'data/templates.xml',
        'views/notification_team_approver_views.xml',
    ],
    'depends': ['add_notification_request_approval_route', 'purchase_requisition_validation_extends'],
    'installable': True,
    'auto_install': True,
    'application': False,
}
